--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg100+1)
-- Dumped by pg_dump version 11.3 (Debian 11.3-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: units_db; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA units_db;


ALTER SCHEMA units_db OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cpu_type; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.cpu_type (
    id integer NOT NULL,
    type character(100) NOT NULL
);


ALTER TABLE units_db.cpu_type OWNER TO postgres;

--
-- Name: CPU_type_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db."CPU_type_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db."CPU_type_id_seq" OWNER TO postgres;

--
-- Name: CPU_type_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db."CPU_type_id_seq" OWNED BY units_db.cpu_type.id;


--
-- Name: os; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.os (
    id integer NOT NULL,
    name character(50) NOT NULL
);


ALTER TABLE units_db.os OWNER TO postgres;

--
-- Name: OS_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db."OS_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db."OS_id_seq" OWNER TO postgres;

--
-- Name: OS_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db."OS_id_seq" OWNED BY units_db.os.id;


--
-- Name: ram_size; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.ram_size (
    id integer NOT NULL,
    size integer NOT NULL
);


ALTER TABLE units_db.ram_size OWNER TO postgres;

--
-- Name: RAM_size_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db."RAM_size_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db."RAM_size_id_seq" OWNER TO postgres;

--
-- Name: RAM_size_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db."RAM_size_id_seq" OWNED BY units_db.ram_size.id;


--
-- Name: licences; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.licences (
    id integer NOT NULL,
    key text NOT NULL
);


ALTER TABLE units_db.licences OWNER TO postgres;

--
-- Name: licence_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db.licence_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db.licence_id_seq OWNER TO postgres;

--
-- Name: licence_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db.licence_id_seq OWNED BY units_db.licences.id;


--
-- Name: locations; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.locations (
    id integer NOT NULL,
    name character(100)
);


ALTER TABLE units_db.locations OWNER TO postgres;

--
-- Name: locations_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db.locations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db.locations_id_seq OWNER TO postgres;

--
-- Name: locations_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db.locations_id_seq OWNED BY units_db.locations.id;


--
-- Name: screen_size; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.screen_size (
    id integer NOT NULL,
    size integer NOT NULL
);


ALTER TABLE units_db.screen_size OWNER TO postgres;

--
-- Name: screen_size_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db.screen_size_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db.screen_size_id_seq OWNER TO postgres;

--
-- Name: screen_size_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db.screen_size_id_seq OWNED BY units_db.screen_size.id;


--
-- Name: storage_capacity; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.storage_capacity (
    id integer NOT NULL,
    capacity integer NOT NULL
);


ALTER TABLE units_db.storage_capacity OWNER TO postgres;

--
-- Name: storage_capacity_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db.storage_capacity_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db.storage_capacity_id_seq OWNER TO postgres;

--
-- Name: storage_capacity_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db.storage_capacity_id_seq OWNED BY units_db.storage_capacity.id;


--
-- Name: unit_type; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.unit_type (
    id integer NOT NULL,
    name character(100),
    related_to text[]
);


ALTER TABLE units_db.unit_type OWNER TO postgres;

--
-- Name: units; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.units (
    id integer NOT NULL,
    acquisition_date date,
    price integer,
    entry_date date DEFAULT (now())::date NOT NULL,
    serial_number text NOT NULL,
    description text,
    start_warranty_date date,
    warranty_duration integer,
    physical_location character(100),
    id_unit_type integer NOT NULL,
    id_vendor integer NOT NULL,
    id_os integer,
    id_storage_capacity integer,
    id_screen_size integer,
    id_ram_size integer,
    id_cpu_type integer,
    id_given_to integer,
    licence text
);


ALTER TABLE units_db.units OWNER TO postgres;

--
-- Name: units_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db.units_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db.units_id_seq OWNER TO postgres;

--
-- Name: units_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db.units_id_seq OWNED BY units_db.units.id;


--
-- Name: units_table_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db.units_table_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db.units_table_id_seq OWNER TO postgres;

--
-- Name: units_table_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db.units_table_id_seq OWNED BY units_db.unit_type.id;


--
-- Name: users; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.users (
    id integer NOT NULL,
    name character(100) NOT NULL
);


ALTER TABLE units_db.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db.users_id_seq OWNED BY units_db.users.id;


--
-- Name: vendor; Type: TABLE; Schema: units_db; Owner: postgres
--

CREATE TABLE units_db.vendor (
    id integer NOT NULL,
    name character(100)
);


ALTER TABLE units_db.vendor OWNER TO postgres;

--
-- Name: vendor_id_seq; Type: SEQUENCE; Schema: units_db; Owner: postgres
--

CREATE SEQUENCE units_db.vendor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_db.vendor_id_seq OWNER TO postgres;

--
-- Name: vendor_id_seq; Type: SEQUENCE OWNED BY; Schema: units_db; Owner: postgres
--

ALTER SEQUENCE units_db.vendor_id_seq OWNED BY units_db.vendor.id;


--
-- Name: cpu_type id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.cpu_type ALTER COLUMN id SET DEFAULT nextval('units_db."CPU_type_id_seq"'::regclass);


--
-- Name: licences id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.licences ALTER COLUMN id SET DEFAULT nextval('units_db.licence_id_seq'::regclass);


--
-- Name: locations id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.locations ALTER COLUMN id SET DEFAULT nextval('units_db.locations_id_seq'::regclass);


--
-- Name: os id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.os ALTER COLUMN id SET DEFAULT nextval('units_db."OS_id_seq"'::regclass);


--
-- Name: ram_size id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.ram_size ALTER COLUMN id SET DEFAULT nextval('units_db."RAM_size_id_seq"'::regclass);


--
-- Name: screen_size id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.screen_size ALTER COLUMN id SET DEFAULT nextval('units_db.screen_size_id_seq'::regclass);


--
-- Name: storage_capacity id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.storage_capacity ALTER COLUMN id SET DEFAULT nextval('units_db.storage_capacity_id_seq'::regclass);


--
-- Name: unit_type id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.unit_type ALTER COLUMN id SET DEFAULT nextval('units_db.units_table_id_seq'::regclass);


--
-- Name: units id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.units ALTER COLUMN id SET DEFAULT nextval('units_db.units_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.users ALTER COLUMN id SET DEFAULT nextval('units_db.users_id_seq'::regclass);


--
-- Name: vendor id; Type: DEFAULT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.vendor ALTER COLUMN id SET DEFAULT nextval('units_db.vendor_id_seq'::regclass);


--
-- Data for Name: cpu_type; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.cpu_type VALUES (1, 'pentium 3                                                                                           ');
INSERT INTO units_db.cpu_type VALUES (2, 'amrv7                                                                                               ');
INSERT INTO units_db.cpu_type VALUES (3, 'test                                                                                                ');


--
-- Data for Name: licences; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.licences VALUES (1, 'qsfdgqsfdg');


--
-- Data for Name: locations; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.locations VALUES (1, 'bureau1                                                                                             ');
INSERT INTO units_db.locations VALUES (2, 'bureau2                                                                                             ');
INSERT INTO units_db.locations VALUES (3, 'inconnue                                                                                            ');
INSERT INTO units_db.locations VALUES (4, 'cuisine                                                                                             ');
INSERT INTO units_db.locations VALUES (5, 'bureau8                                                                                             ');
INSERT INTO units_db.locations VALUES (6, 'test                                                                                                ');
INSERT INTO units_db.locations VALUES (7, 'test2                                                                                               ');
INSERT INTO units_db.locations VALUES (8, 'buanderie                                                                                           ');
INSERT INTO units_db.locations VALUES (9, 'test5                                                                                               ');
INSERT INTO units_db.locations VALUES (10, 'test4                                                                                               ');
INSERT INTO units_db.locations VALUES (11, 'test11                                                                                              ');


--
-- Data for Name: os; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.os VALUES (1, 'GNU/Linux                                         ');
INSERT INTO units_db.os VALUES (2, 'Windows                                           ');
INSERT INTO units_db.os VALUES (3, 'Sans                                              ');
INSERT INTO units_db.os VALUES (4, 'ios                                               ');


--
-- Data for Name: ram_size; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.ram_size VALUES (1, 2);
INSERT INTO units_db.ram_size VALUES (2, 4);
INSERT INTO units_db.ram_size VALUES (3, 32);
INSERT INTO units_db.ram_size VALUES (4, 12);


--
-- Data for Name: screen_size; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.screen_size VALUES (1, 24);
INSERT INTO units_db.screen_size VALUES (2, 17);
INSERT INTO units_db.screen_size VALUES (3, 22);


--
-- Data for Name: storage_capacity; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.storage_capacity VALUES (1, 16);
INSERT INTO units_db.storage_capacity VALUES (2, 500);
INSERT INTO units_db.storage_capacity VALUES (3, 6000);


--
-- Data for Name: unit_type; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.unit_type VALUES (1, 'Divers                                                                                              ', '{}');
INSERT INTO units_db.unit_type VALUES (2, 'Ordinateur portable                                                                                 ', '{os,screen_size,ram_size,cpu_type,storage_capacity,licences}');


--
-- Data for Name: units; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.units VALUES (9, NULL, NULL, '2019-06-19', 'sdfgsdfgh', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (10, NULL, NULL, '2019-06-20', '1243', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (11, '2019-06-13', 13, '2019-06-20', 'çsfd_gç98998', 'super matos !', '2019-06-13', 15, '1                                                                                                   ', 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (12, '2019-06-18', 4, '2019-06-20', 'sdgfhsd', 'super matos de !!!!', '2019-06-05', 5, '1                                                                                                   ', 2, 2, 1, 1, 1, 1, 2, NULL, NULL);
INSERT INTO units_db.units VALUES (13, '2019-06-12', 8, '2019-06-20', 'dsfgdf', 'hello, world', '2019-06-12', 10, '2                                                                                                   ', 2, 2, 1, 2, 2, 2, 1, 2, NULL);
INSERT INTO units_db.units VALUES (14, NULL, NULL, '2019-06-20', 'wsdfg', 'sqdfsdf', '2019-06-13', NULL, '1                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (15, NULL, NULL, '2019-06-20', 'qsdfg', NULL, '2019-06-13', NULL, '1                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (16, '2019-06-13', 6, '2019-06-20', 'test1', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (17, '2019-06-13', 6, '2019-06-20', 'test2', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (18, '2019-06-13', 6, '2019-06-20', 'test3', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (19, '2019-06-13', 6, '2019-06-20', 'test4', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (20, '2019-06-13', 6, '2019-06-20', 'test5', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (21, '2019-06-13', 6, '2019-06-20', 'test6', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (22, '2019-06-13', 6, '2019-06-20', 'test7', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (23, '2019-06-13', 6, '2019-06-20', 'test7', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (24, '2019-06-13', 6, '2019-06-20', 'test8', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (25, '2019-06-13', 6, '2019-06-20', 'test9', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (26, '2019-06-13', 6, '2019-06-20', 'test10', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (27, '2019-06-13', 6, '2019-06-20', 'test11', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (28, '2019-06-13', 6, '2019-06-20', 'test12', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (29, '2019-06-13', 6, '2019-06-20', 'test13', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (30, '2019-06-13', 6, '2019-06-20', 'test13', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (31, '2019-06-13', 6, '2019-06-20', 'test14', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (32, '2019-06-13', 6, '2019-06-20', 'test15', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (33, '2019-06-13', 6, '2019-06-20', 'test16', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (34, '2019-06-13', 6, '2019-06-20', 'test16', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (35, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (36, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (37, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (38, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (39, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (40, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (41, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (42, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (43, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (44, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (45, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (46, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (47, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (48, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (49, '2019-06-13', 6, '2019-06-20', 'test17', 'testtt', '2019-06-04', 6, '2                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO units_db.units VALUES (50, NULL, NULL, '2019-06-20', 'blopy', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (51, NULL, NULL, '2019-06-21', 'bdv', NULL, NULL, 0, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (52, NULL, NULL, '2019-06-21', 'sqdfs', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (53, NULL, NULL, '2019-06-21', 'dddqsdf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (54, NULL, NULL, '2019-06-21', 'qsd', NULL, NULL, NULL, '3                                                                                                   ', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (55, NULL, NULL, '2019-06-21', 'ddd', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (56, NULL, NULL, '2019-06-21', 'qsdgf', NULL, NULL, 0, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (57, NULL, NULL, '2019-06-21', 'qsdf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (58, NULL, 2, '2019-06-21', 'wsdfg', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (59, NULL, NULL, '2019-06-21', 'qsdf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (60, NULL, NULL, '2019-06-21', '<qsdf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (61, NULL, NULL, '2019-06-21', 'dfg', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (62, NULL, NULL, '2019-06-21', 'qsdfg', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (63, NULL, NULL, '2019-06-21', 'qsdf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (64, NULL, NULL, '2019-06-21', 'qsdf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (65, NULL, NULL, '2019-06-21', 'qsdfg', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (66, NULL, NULL, '2019-06-21', 'qsdf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (67, NULL, NULL, '2019-06-21', 'w<sd', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (68, NULL, NULL, '2019-06-21', 'df', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (69, NULL, NULL, '2019-06-21', 'sqd', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (70, NULL, NULL, '2019-06-21', 'sqdcs', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (71, NULL, NULL, '2019-06-21', 'qsdfdgv', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (72, NULL, NULL, '2019-06-21', 'wsfv', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (73, NULL, NULL, '2019-06-21', 'qsd', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (74, NULL, NULL, '2019-06-21', 'zsdsf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (75, NULL, NULL, '2019-06-21', 'qqszdf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (76, NULL, NULL, '2019-06-21', 'qsdfv', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (77, NULL, NULL, '2019-06-21', 'zsdef', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (78, NULL, NULL, '2019-06-21', 'qsdf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (79, NULL, NULL, '2019-06-21', 'qsdfqsd', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (80, NULL, NULL, '2019-06-21', 'qsdgfqsf', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (81, NULL, NULL, '2019-06-21', 'sdfqsd', NULL, NULL, NULL, '3                                                                                                   ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (82, NULL, NULL, '2019-06-21', '11', NULL, NULL, NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (83, NULL, NULL, '2019-06-21', '111', NULL, NULL, NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (84, NULL, NULL, '2019-06-21', '11', NULL, NULL, NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (85, NULL, NULL, '2019-06-21', '11', NULL, NULL, NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (86, NULL, NULL, '2019-06-21', 'dfghsdfgh', NULL, NULL, NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (87, NULL, NULL, '2019-06-21', '12433', NULL, NULL, NULL, NULL, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO units_db.units VALUES (88, NULL, NULL, '2019-06-21', 'blop', NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- Data for Name: users; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.users VALUES (1, 'Bob                                                                                                 ');
INSERT INTO units_db.users VALUES (2, 'Alice                                                                                               ');
INSERT INTO units_db.users VALUES (3, 'Maurice                                                                                             ');
INSERT INTO units_db.users VALUES (4, 'Rachid                                                                                              ');


--
-- Data for Name: vendor; Type: TABLE DATA; Schema: units_db; Owner: postgres
--

INSERT INTO units_db.vendor VALUES (1, 'inconnu                                                                                             ');
INSERT INTO units_db.vendor VALUES (2, 'ldlc                                                                                                ');
INSERT INTO units_db.vendor VALUES (3, 'Carrefour                                                                                           ');
INSERT INTO units_db.vendor VALUES (4, 'test                                                                                                ');
INSERT INTO units_db.vendor VALUES (5, 'wich                                                                                                ');


--
-- Name: CPU_type_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db."CPU_type_id_seq"', 3, true);


--
-- Name: OS_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db."OS_id_seq"', 4, true);


--
-- Name: RAM_size_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db."RAM_size_id_seq"', 4, true);


--
-- Name: licence_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db.licence_id_seq', 1, true);


--
-- Name: locations_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db.locations_id_seq', 11, true);


--
-- Name: screen_size_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db.screen_size_id_seq', 3, true);


--
-- Name: storage_capacity_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db.storage_capacity_id_seq', 3, true);


--
-- Name: units_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db.units_id_seq', 88, true);


--
-- Name: units_table_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db.units_table_id_seq', 2, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db.users_id_seq', 4, true);


--
-- Name: vendor_id_seq; Type: SEQUENCE SET; Schema: units_db; Owner: postgres
--

SELECT pg_catalog.setval('units_db.vendor_id_seq', 5, true);


--
-- Name: cpu_type CPU_type_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.cpu_type
    ADD CONSTRAINT "CPU_type_pkey" PRIMARY KEY (id);


--
-- Name: os OS_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.os
    ADD CONSTRAINT "OS_pkey" PRIMARY KEY (id);


--
-- Name: ram_size RAM_size_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.ram_size
    ADD CONSTRAINT "RAM_size_pkey" PRIMARY KEY (id);


--
-- Name: licences licence_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.licences
    ADD CONSTRAINT licence_pkey PRIMARY KEY (id);


--
-- Name: locations locations_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: screen_size screen_size_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.screen_size
    ADD CONSTRAINT screen_size_pkey PRIMARY KEY (id);


--
-- Name: storage_capacity storage_capacity_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.storage_capacity
    ADD CONSTRAINT storage_capacity_pkey PRIMARY KEY (id);


--
-- Name: units units_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.units
    ADD CONSTRAINT units_pkey PRIMARY KEY (id);


--
-- Name: unit_type units_table_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.unit_type
    ADD CONSTRAINT units_table_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: vendor vendor_pkey; Type: CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.vendor
    ADD CONSTRAINT vendor_pkey PRIMARY KEY (id);


--
-- Name: units id_cpu_type_fkey; Type: FK CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.units
    ADD CONSTRAINT id_cpu_type_fkey FOREIGN KEY (id_cpu_type) REFERENCES units_db.cpu_type(id);


--
-- Name: units id_given_to_fkey; Type: FK CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.units
    ADD CONSTRAINT id_given_to_fkey FOREIGN KEY (id_given_to) REFERENCES units_db.users(id);


--
-- Name: units id_ram_size_fkey; Type: FK CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.units
    ADD CONSTRAINT id_ram_size_fkey FOREIGN KEY (id_ram_size) REFERENCES units_db.ram_size(id);


--
-- Name: units id_screen_size_capacity_fkey; Type: FK CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.units
    ADD CONSTRAINT id_screen_size_capacity_fkey FOREIGN KEY (id_screen_size) REFERENCES units_db.screen_size(id);


--
-- Name: units id_storage_capacity_fkey; Type: FK CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.units
    ADD CONSTRAINT id_storage_capacity_fkey FOREIGN KEY (id_storage_capacity) REFERENCES units_db.storage_capacity(id);


--
-- Name: units id_unit_type_fkey; Type: FK CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.units
    ADD CONSTRAINT id_unit_type_fkey FOREIGN KEY (id_unit_type) REFERENCES units_db.unit_type(id);


--
-- Name: units id_vendor_fkey; Type: FK CONSTRAINT; Schema: units_db; Owner: postgres
--

ALTER TABLE ONLY units_db.units
    ADD CONSTRAINT id_vendor_fkey FOREIGN KEY (id_vendor) REFERENCES units_db.vendor(id);


--
-- PostgreSQL database dump complete
--

