let containerFormEl  = document.getElementById('containerForm');

function createEventListenersAndHandleModal() {
    const selectEls = document.querySelectorAll("select");

    for (const select of selectEls) {
	select.addEventListener('change', function(event) {
	    if (event.target.value == "add"){
		console.log(event.target.name);

		// Modal management
		var modal = document.getElementById("addModal");
		var span = document.getElementsByClassName("close")[0];
		modal.style.display = "block";

		fetch(
		    'partials/add-form-modal.php?table=' + event.target.name,
		    {method: 'GET'})
		    .then(function(response) {
			response.text()
			    .then(function(text) {
				containerFormEl.innerHTML = text;

				// Handle the form inside the modal
				function sendData() {
				    var XHR = new XMLHttpRequest();
				    // Bind the FormData object and the form element
				    var FD = new FormData(form);
				    // Define what happens on successful data submission
				    XHR.addEventListener("load", function(event) {
					// successMessageEl.style.display = "block";
					// alert(event.target.responseText);
					modal.style.display = "none";
					location.reload();
				    });
				    // Define what happens in case of error
				    XHR.addEventListener("error", function(event) {
					alert('Oops! Something went wrong.');
				    });
				    // Set up our request
				    XHR.open("POST", "../controller/processing/insert-types.php");
				    // The data sent is what the user provided in the form
				    XHR.send(FD);
				}
				
				// Access the form element...
				var form = document.getElementById("addTypeModal");
				console.log(form);

				// ...and take over its submit event.
				form.addEventListener("submit", function (event) {
				    event.preventDefault();
				    sendData();
				});
				

			    });
		    });

		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.display = "none";
		};
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
		    if (event.target == modal) {
			modal.style.display = "none";
		    }
		};

	    }
	});
    }

}

createEventListenersAndHandleModal();
