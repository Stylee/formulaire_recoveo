let idUnitTypeEl = document.querySelector("select[name='unit_type']");
var elToInsertBefore = document.getElementById("bonus-fields-separation");

let elDisplayMoreFieldsButton = document.getElementById('DisplayMoreFieldsButton');

// Do not show the "Afficher plus de champs" button if idUnitTypeEl.value is null
if ( idUnitTypeEl.value == '' || idUnitTypeEl.value == "add") {
    elDisplayMoreFieldsButton.style.display = 'none';
}

elDisplayMoreFieldsButton.addEventListener("click", fetchAndDisplayFields, false);
idUnitTypeEl.addEventListener("change", toggleBonusFieldsButtonVisibility, false);

function doesFieldCanDisplayBonusFields() {
    if ( idUnitTypeEl.value != null && idUnitTypeEl.value != "add") {
	return true;
    } else {
	return false;
    }
}

function toggleBonusFieldsButtonVisibility () {
    if ( doesFieldCanDisplayBonusFields() ) {
	elDisplayMoreFieldsButton.style.display = 'inline-block';
    } else {
	elDisplayMoreFieldsButton.style.display = 'none';
    }
}

function fetchAndDisplayFields() {
    elDisplayMoreFieldsButton.style.display = "none";

    if (doesFieldCanDisplayBonusFields()) {
    fetch(
	'partials/display_fields_related_to_unit_type.php?id_unit_type=' + idUnitTypeEl.value,
	{method: 'GET'})
	.then(function(response) {
            response.text()
		.then(function(text) {
		    // Removing bonus fields from the DOM
		    var elsToRemove = document.querySelectorAll('.bonus-field');
		    for (i = 0; i < elsToRemove.length; i++) {
			elsToRemove[i].parentNode.removeChild(elsToRemove[i]);
			console.log(elsToRemove[i]);
		    }

		    // Inserting bonus related to unit type
		    elToInsertBefore.insertAdjacentHTML("afterend", text);
		    createEventListenersAndHandleModal(); // function from add-modal.js
		});
	});
    }
    // Now let's always display bonus fields
    idUnitTypeEl.addEventListener("change", fetchAndDisplayFields, false);
}
    

