<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require $path . '/conf/conf.php';

$UnitsManager = new UnitsManager($bdd);
$DBManager = new DBManager($bdd);

$idUnitType = $_POST['unit_type'];
$serial_number = $_POST['serial_number'];

if ($DBManager->isSNTaken($serial_number)){
    // This string get checked in add-unit.js
    echo "Le Numéro de série est déjà attribué";
    die();
}


$id_location = $_POST['location'];
$id_vendor = $_POST['vendor'];

if (isset($_POST['user'])) {
    $id_user = $_POST['users'];
} else  {
    $id_user = null;
}

if (isset($_POST['buy_date']) AND ($_POST['buy_date'] != '')) {
    $buy_date = $_POST['buy_date'];
} else  {
    $buy_date = null;
}

if (isset($_POST['start_warranty_date']) AND ($_POST['start_warranty_date'] != '')){
    $start_warranty_date = $_POST['start_warranty_date'];
} else {
    $start_warranty_date = null;
}

if (isset($_POST['warranty_duration']) AND ($_POST['warranty_duration'] != '')){
    $warranty_duration = $_POST['warranty_duration'];
} else {
    $warranty_duration = null;
}

if (isset($_POST['description']) AND ($_POST['description'] != '')) {
    $description = $_POST['description'];
} else {
    $description = null;
}

if (isset($_POST['os'])) {
    $idOS = $_POST['os'];
} else {
    $idOS= null;
}

if (isset($_POST['price'])) {
    $price = $_POST['price'];
} else {
    $price = null;
}

if (isset($_POST['user'])) {
    $idGivenTo = $_POST['user'];
} else {
    $idGivenTo = null;
}

if (isset($_POST['storage_capacity'])) {
    $idStorageCapacity = $_POST['storage_capacity'];
} else {
    $idStorageCapacity = null;
}

if (isset($_POST['screen_size'])) {
    $idScreenSize = $_POST['screen_size'];
} else {
    $idScreenSize = null;
}

if (isset($_POST['ram_size'])) {
    $idRAMSize = $_POST['ram_size'];
} else {
    $idRAMSize = null;
}

if (isset($_POST['cpu_type'])) {
    $idCPUType = $_POST['cpu_type'];
} else {
    $idCPUType = null;
}

$Unit = new Unit(
    [
	'SerialNumber' => "$serial_number",
	'PhysicalLocation' => $id_location ,
	'WarrantyDuration' => $warranty_duration,
	'StartWarrantyDate' => $start_warranty_date,
	'Description' => $description,
	'AcquisitionDate' => $buy_date,
	'Price' => $price,
	'IdUnitType' => $idUnitType,
	'IdVendor' => $id_vendor,
	'IdOS' => $idOS,
	'IdStorageCapacity' => $idStorageCapacity,
	'IdScreenSize' => $idScreenSize,
	'IdRAMSize' => $idRAMSize,
	'IdCPUType' => $idCPUType,
	'IdGivenTo' => $idGivenTo,
    ]
);

$UnitsManager->addUnitToDB($Unit);
?>
