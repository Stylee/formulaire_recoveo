<?php
class UnitsManager
{
    private $bdd;

    // bdd
    private function bdd()
    {
        return $this->bdd;
    }

    public function __construct(PDO $bdd)
    {
        $this->bdd = $bdd;
    }

    // CRUD
    public function addUnitToDB(Unit $Unit)
    {
        $data = [
            'acquisition_date' => $Unit->getAcquisitionDate(),
            'price' => $Unit->getPrice(),
            'serial_number' => $Unit->getSerialNumber(),
            'description' => $Unit->getDescription(),
            'start_warranty_date' => $Unit->getStartWarrantyDate(),
            'warranty_duration' => $Unit->getWarrantyDuration(),
            'physical_location' => $Unit->getPhysicalLocation(),
            'id_unit_type' => $Unit->getIdUnitType(),
            'id_vendor' => $Unit->getIdVendor(),
            'id_os' => $Unit->getIdOS(),
            'id_storage_capacity' => $Unit->getIdStorageCapacity(),
            'id_screen_size' => $Unit->getIdScreenSize(),
            'id_ram_size' => $Unit->getIdRAMSize(),
            'id_cpu_type' => $Unit->getIdCPUType(),
            'id_given_to' => $Unit->getIdGivenTo()
        ];

        $sql = "INSERT INTO units_db.units (
                    acquisition_date,
                    price,
                    serial_number,
                    description,
                    start_warranty_date,
                    warranty_duration,
                    physical_location,
                    id_unit_type,
                    id_vendor,
                    id_os,
                    id_storage_capacity,
                    id_screen_size,
                    id_ram_size,
                    id_cpu_type,
                    id_given_to
                    )
                VALUES(
                    :acquisition_date,
                    :price,
                    :serial_number,
                    :description,
                    :start_warranty_date,
                    :warranty_duration,
                    :physical_location,
                    :id_unit_type,
                    :id_vendor,
                    :id_os,
                    :id_storage_capacity,
                    :id_screen_size,
                    :id_ram_size,
                    :id_cpu_type,
                    :id_given_to
                    )";
        $req = $this->bdd->prepare($sql);
        $req->execute($data);
    }

    public function updateUnit(Unit $Unit)
    {
	/* TODO */
	
	/* 
	 *         $sql = "
	 *             UPDATE units_db.units
	 *              SET
	 *                serial_number = '$Unit->getSerialNumber()'
	 *              WHERE
	 *                id = $Unit->getId();";
	 * 
	 *         $req = $this->bdd->prepare($sql);
	 *         $req->execute();
	 *  */
    }


    // Getters
    public function getIdFromSerialNumber($serial_number)
    {
        $req = $this->bdd()->prepare("SELECT id FROM units_db.units WHERE serial_number = '$serial_number'");
        $req->execute();
        return $req->fetch();
    }

    public function getAllUnitType()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.unit_type');
        $req->execute();
        return $req->fetchAll();
    }

    public function getAllLocations()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.locations');
        $req->execute();
        return $req->fetchAll();
    }
    
    public function getAllUsers()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.users');
        $req->execute();
        return $req->fetchAll();
    }

    public function getAllOS()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.os');
        $req->execute();
        return $req->fetchAll();
    }

    public function getAllVendors()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.vendor');
        $req->execute();
        return $req->fetchAll();
    }

    
    public function getAllScreenSize()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.screen_size');
        $req->execute();
        return $req->fetchAll();
    }

    public function getAllRamSize()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.ram_size');
        $req->execute();
        return $req->fetchAll();
    }

    public function getAllCpuType()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.cpu_type');
        $req->execute();
        return $req->fetchAll();
    }

    public function getAllStorageCapacities()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.storage_capacity');
        $req->execute();
        return $req->fetchAll();
    }

    public function getFieldsRelatedToUnitType($id_unit_type)
    {
        // $sql = "SELECT array_to_json(related_to) AS test FROM units_db.unit_type WHERE id = $id_unit_type;";
        $sql = "SELECT related_to FROM units_db.unit_type WHERE id = $id_unit_type";
        $req = $this->bdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function getAllLicences()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.licences');
        $req->execute();
        return $req->fetchAll();
    }
    
    public function getAllUNits()
    {
        $req = $this->bdd()->prepare('SELECT * FROM units_db.units');
        $req->execute();
        return $req->fetchAll();
    }
    
}
