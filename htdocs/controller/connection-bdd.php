<?php
try {
    $bdd = new PDO('pgsql:host=10.0.3.5;port=5432;dbname=postgres;user=postgres');
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $bdd->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch (Exception $err) {
    die('Erreur: ' . $err->getMessage());
}
?>
