<?php
class DBManager
{
    private $bdd;

    public function __construct(PDO $bdd)
    {
        $this->bdd = $bdd;
    }

    public function getTablesNames()
    {
        $sql = "
            SELECT table_name
              FROM information_schema.tables
            WHERE table_schema='units_db'
              AND table_type='BASE TABLE';";
        $req = $this->bdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function getColumns($table) {

        $sql = "select column_name,data_type 
	from information_schema.columns 
	where table_name = '$table';";

        $req = $this->bdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();

	}

    public function insertLocations(string $location)
    {
        $data = ['name' => $location];
        $sql = "INSERT INTO units_db.locations (name)
                VALUES(:name)";
        $req = $this->bdd->prepare($sql);
        $req->execute($data);
    }

    public function insertVendor(string $vendor)
    {
        $data = ['name' => $vendor];
        $sql = "INSERT INTO units_db.vendor (name)
                VALUES(:name)";
        $req = $this->bdd->prepare($sql);
        $req->execute($data);
    }

    public function insertUsers(string $users)
    {
        $data = ['name' => $users];
        $sql = "INSERT INTO units_db.users (name)
                VALUES(:name)";
        $req = $this->bdd->prepare($sql);
        $req->execute($data);
    }

    public function insertOs(string $os)
    {
        $data = ['name' => $os];
        $sql = "INSERT INTO units_db.OS (name)
                VALUES(:name)";
        $req = $this->bdd->prepare($sql);
        $req->execute($data);
    }

    public function insertScreen_Size(string $screen_size)
    {
        $data = ['size' => $screen_size];
        $sql = "INSERT INTO units_db.screen_size (size)
                VALUES(:size)";
        $req = $this->bdd->prepare($sql);
        $req->execute($data);
    }

    public function insertRam_Size(string $ram_size)
    {
        $data = ['size' => $ram_size];
        $sql = "INSERT INTO units_db.ram_size (size)
                VALUES(:size)";
        $req = $this->bdd->prepare($sql);
        $req->execute($data);
    }

    public function insertCpu_Type(string $cpu_type)
    {
        $data = ['type' => $cpu_type];
        $sql = "INSERT INTO units_db.cpu_type (type)
                VALUES(:type)";
        $req = $this->bdd->prepare($sql);
        $req->execute($data);
    }

    public function insertStorage_Capacity(string $storage_capacity)
    {
        $data = ['capacity' => $storage_capacity];
        $sql = "INSERT INTO units_db.storage_capacity (capacity)
                VALUES(:capacity)";
        $req = $this->bdd->prepare($sql);
        $req->execute($data);
    }

    public function insertLicences(string $licences)
    {
        $data = ['key' => $licences];
        $sql = "INSERT INTO units_db.licences (key)
                VALUES(:key)";
        $req = $this->bdd->prepare($sql);
        $req->execute($data);
    }
    
    public function isSNTaken($serial_number)
    {
        $req = $this->bdd()->prepare("SELECT id FROM units_db.units WHERE serial_number = '$serial_number'");
        $req->execute();
        
        if ($req->fetch() != NULL) {
            return true;
        } else {
            return false;
        }
    }

    
	// bdd
	private function bdd()
	{
        return $this->bdd;
	}

}
