<main>

    <header>
	<h1><?php echo $pagetitle ?></h1>
    </header>
    
    <div class="container-btn-index">
	<a href="view/ajout.php" class="btn btn-big">Ajouter une entrée</a>

	<form action="view/modification.php" class="vertical-form">
	    <label for="serial_number_nav">Numéro de Série:</label>
	    <input name="serial_number_nav" type="text" placeholder="4554D6D56S3Z"/>
	    <button type="submit" class="btn btn-big">Modifier une entrée</button>
	</form>

	<a href="view/statistiques.php" class="btn btn-big">Statistiques</a>
    </div>
    
</main>



