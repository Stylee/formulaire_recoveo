<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require $path . '/conf/conf.php';
?>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
	<title><?= $pagetitle ?></title>
	<link rel="stylesheet" href="/assets/styles/css/main.css">
    </head>

    <body>
	<?php require $tpl ?>

    </body>


</html>
