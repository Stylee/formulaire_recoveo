<?php

$UnitsManager = new UnitsManager($bdd);

$allUnitTypes = $UnitsManager->getAllUnitType();
$allLocations = $UnitsManager->getAllLocations();
$allVendors = $UnitsManager->getAllVendors();
$allUsers = $UnitsManager->getAllUsers();

$serialNumber = $_POST['serial_number_nav'];
?>

<header>
    <h1><?php echo $pagetitle ; ?></h1>

    <div class="container-btn-index">
	<a href="ajout.php" type="submit" class="btn btn-medium">Ajouter une entrée</button>
	    <a href="statistiques.php" class="btn btn-big">Statistiques</a>
    </div>

</header>

<main>
    <p>Modification de l'unité avec le numéro de série <?= $serialNumber ?></p>
    
    <form id="addUnitForm" action="../../controller/processing/update-unit.php"
	  method="POST" class="container-fields">

	<div class="container-field">
	    <h3>Type de Matériel</h3>
	    <select name="unit_type" required>
		<option hidden disabled selected value>-- Selectionnez un type --</option>
		<option value="add">### Ajouter ###</option>
		<?php foreach ($allUnitTypes as $unitType): ?>
		    <option value="<?= $unitType["id"]?>"><?= $unitType["name"] ;?></option>
		<?php endforeach ?>
	    </select>
	</div>

	<div class="container-field">
	    <h3>Numéro de Série</h3>
	    <label for="serial_number_nav">Numéro de Série:</label>
	    <input name="serial_number" type="text" placeholder="4554D6D56S3Z" required/>
	</div>

	<div class="container-field">
	    <h3>Où se trouve le matériel</h3>
	    <select name="locations">
		<option value="3" selected>Inconnue</option>
		<option value="add">### Ajouter ###</option>
		<?php foreach ($allLocations as $location): ?>
		    <?php if ($location['id'] != 3): ?>
		    	<option value="<?= $location["id"]?>"><?= $location["name"] ;?></option>
		    <?php endif ?>
		<?php endforeach ?>
	    </select>
	</div>

	<div class="container-field">
	    <h3>Vendeur</h3>
	    <select name="vendor">
		<option value="1" selected>Inconnu</option>
		<option value="add">### Ajouter ###</option>
		<?php foreach ($allVendors as $vendor): ?>
		    <?php if ($vendor['id'] != 1): ?>
			<option value="<?= $vendor["id"]?>"><?= $vendor["name"] ;?></option>
		    <?php endif ?>
		<?php endforeach ?>
	    </select>
	</div>

	<div class="container-field">
	    <h3>Attribué à</h3>
	    <select name="users">
		<option hidden disabled selected value>-- Selectionnez une personne --</option>
		<option value="add">### Ajouter ###
		    <?php foreach ($allUsers as $user): ?>
			<option value="<?= $user["id"]?>"><?= $user["name"] ;?></option>
		    <?php endforeach ?>
	    </select>
	</div>

	<div class="container-field">
	    <h3>Date d'achat</h3>
	    <input name="buy_date" type="date"/>
	</div>
	
	<div class="container-field">
	    <h3>Prix d'achat</h3>
	    <small>En euros.</small>
	    <input name="price" type="number"/>
	</div>

	<div class="container-field">
	    <h3>Début de la garantie</h3>
	    <input name="start_warranty_date" type="date"/>
	</div>

	<div class="container-field">
	    <h3>Durée de la garantie</h3>
	    <small>En mois.</small>
	    <input name="warranty_duration" type="number"/>
	</div>

	<div class="container-field">
	    <h3>Description / Commentaire</h3>
	    <textarea name="description" type="text"></textarea>
	</div>

	<div id="bonus-fields-separation">
	    <hr/>
	</div>

	<div id="successMessage">
	    Le matériel à bien été ajouté.
	</div>
	
	<button id="addUnitSubmitButton" type="submit" class="btn btn-big">Envoyer</button>

	<div id="DisplayMoreFieldsButton" class="btn btn-medium">Afficher plus de champs</div>

	
    </form>

</main>

<!-- The Modal -->
<div id="addModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
	<span class="close">&times;</span>

	<div id="containerForm">
	</div>
	
    </div>

</div> 

<script src="../../assets/scripts/js/add-and-handle-modal.js"></script>
<script src="../../assets/scripts/js/bonus-fields-management.js"></script>
<script src="../../assets/scripts/js/update-unit.js"></script>


