<?php

$UnitsManager = new UnitsManager($bdd);

$allUnits = $UnitsManager->getAllUnits();
$DBManager = new DBManager($bdd);

$columns = $DBManager->getColumns('units');
?>

<header>
    <h1><?php echo $pagetitle ; ?></h1>

    <div class="container-btn-index">
	<a href="ajout.php" class="btn btn-big">Ajouter une entrée</a>
	<form action="modification.php" class="vertical-form" method="POST">
	    <label for="serial_number_nav">Numéro de Série:</label>
	    <input name="serial_number_nav" type="text" placeholder="4554D6D56S3Z"/>
	    <button type="submit" class="btn btn-medium">Modifier une entrée</button>
	</form>
    </div>

   
</header>

<main>
    <?php foreach ($allUnits as $unit): ?>
	<div class="container-field">
	    <?php foreach($columns as $column):?>
		<p>
		    <?php
		    $index = $column['column_name'];

		    if ($unit[$index] != NULL AND $unit[$index] != '')
		    {
			echo $index . " = " . $unit[$index];
		    }
		    ?>
		</p>
	    <? endforeach ?>
	</div>
    <? endforeach ?>
</main>
