<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require $path . '/conf/conf.php';

$DBManager =  new DBManager($bdd);
$table = $_GET['table'];
$columns = $DBManager->getColumns($table);
?>

<form action="" id="addTypeModal" method="POST">
    <input name="table" type="text" value="<?=$table?>" hidden/>
    
    <? foreach ($columns as $column) :?>
	<? 
	switch ($column['data_type']) {
	    case "integer":
		$type = "number";
		break;
	    case "character":
		$type = "text";
		break;
	    case "text":
		$type = "text";
		break;
	    default:
		$type = "text";
	}
	?>
	<? if ($column['column_name'] != 'id') : ?>
	    <p><?=$column['column_name']?></p>

	    <input name="<?= $column['column_name'] ?>" type="<?= $type ?>"/>
	<? endif ?>
    <? endforeach ?>

    <button type="submit">Envoyer </button>
</form>
