<?php $allScreenSize = $UnitsManager->getAllScreenSize(); ?>

<div class="container-field bonus-field">
    <h3>Taille d'écran </h3>
    <small>En pouces</small>
    <select name="screen_size">
	<option hidden disabled selected value>-- Selectionnez une taille d'écran --</option>
	<option value="add">### Ajouter ###</option>";
	<?php foreach ($allScreenSize as $screenSize) :  ?>
            <option value="<?= $screenSize['id'] ?>"><?= $screenSize['size'] ?></option>;
	<?php endforeach ?>
    </select>

</div>
