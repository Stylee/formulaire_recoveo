<?php $allStorageCapacities = $UnitsManager->getAllStorageCapacities(); ?>

<div class="container-field bonus-field">
    <h3>Capacité de stockage </h3>
    <small>En Go</small>
    <select name="storage_capacity">
	<option hidden disabled selected value>-- Selectionnez une capacité de stockage --</option>
	<option value="add">### Ajouter ###</option>";
	<?php foreach ($allStorageCapacities as $storageCapacity) :  ?>
            <option value="<?= $storageCapacity['id'] ?>"><?= $storageCapacity['capacity'] ?></option>;
	<?php endforeach ?>
    </select>

</div>
