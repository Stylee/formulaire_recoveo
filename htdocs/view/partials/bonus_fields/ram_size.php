<?php $allRamSize = $UnitsManager->getAllRamSize();  ?>

<div class="container-field bonus-field">
    <h3>Taille de la mémoire vive</h3>
    <small>en Go</small>
    <select name="ram_size">
	<option hidden disabled selected value>-- Selectionnez une taille de RAM --</option>
	<option value="add">### Ajouter ###</option>";
	<?php foreach ($allRamSize as $ramSize) :  ?>
            <option value="<?= $ramSize["id"] ?>"><?= $ramSize["size"] ?></option>;
	<?php endforeach ?>
    </select>
</div>

