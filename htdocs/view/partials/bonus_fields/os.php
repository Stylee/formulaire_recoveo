<?php $allOS = $UnitsManager->getAllOS();  ?>

<div class="container-field bonus-field">
    <h3>Système d'exploitation</h3>
    <select name="os">
	<option hidden disabled selected value>-- Selectionnez un OS --</option>
	<option value="add">### Ajouter ###</option>";
	<?php foreach ($allOS as $OS) :  ?>
            <option value="<?= $OS["id"] ?>"><?= $OS["name"] ?></option>;
	<?php endforeach ?>
   </select>
</div>
