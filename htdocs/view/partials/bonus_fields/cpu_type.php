<?php $allCpuType = $UnitsManager->getAllCpuType(); ?>

<div class="container-field bonus-field">
    <h3>Type de processeur </h3>
    <select name="cpu_type">
	<option hidden disabled selected value>-- Selectionnez un type de processeur --</option>
	<option value="add">### Ajouter ###</option>";
	<?php foreach ($allCpuType as $cpuType) :  ?>
            <option value="<?= $cpuType['id'] ?>"><?= $cpuType['type'] ?></option>;
	<?php endforeach ?>
    </select>

</div>

