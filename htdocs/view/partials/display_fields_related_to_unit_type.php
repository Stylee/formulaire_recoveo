<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require $path . '/conf/conf.php';
$UnitsManager = new UnitsManager($bdd);
$id_unit_type = $_GET['id_unit_type'];

$relatedTo = $UnitsManager->getFieldsRelatedToUnitType($id_unit_type);
$relatedTo = str_replace('{', '', $relatedTo[0]['related_to']);
$relatedTo = str_replace('}', '', $relatedTo);
$relatedTo = explode(',', $relatedTo);

// $array = json_decode($relatedTo);
$i = 0;
foreach ($relatedTo as $characteristic) {
    switch ($characteristic) {
	case "os":
        require $path . '/view/partials/bonus_fields/os.php';
        break;
	case "storage_capacity":
        require $path . '/view/partials/bonus_fields/storage_capacity.php';
        break;
	case "screen_size":
        require $path . '/view/partials/bonus_fields/screen_size.php';
        break;
	case "ram_size":
        require $path . '/view/partials/bonus_fields/ram_size.php';
        break;
	case "cpu_type":
        require $path . '/view/partials/bonus_fields/cpu_type.php';
        break;
    case "licences":
        require $path . '/view/partials/bonus_fields/licences.php';
        break;

    }
}
?>
