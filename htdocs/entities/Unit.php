<?php
class Unit
{
    // DB representation
    private $id;
    private $entry_date;
    private $serial_number;
    private $physical_location;
    private $warranty_duration;
    private $start_warranty_date;
    private $description;
    private $acquisition_date;
    private $price;

    private $id_unit_type;
    private $id_vendor;
    private $id_OS;
    private $id_storage_capacity;
    private $id_screen_size;
    private $id_RAM_size;
    private $id_CPU_type;
    private $id_given_to;

    // Hydratation
    public function __construct(array $data)
    {
	$this->hydrate($data);
    }
    
    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {

            if ($value != null) {
                $method = 'set' . ucfirst($key);
                
                if (method_exists($this, $method)) {
                    $this->$method($value);
                }
            }
        }
    }

    // Setters
    public function setId(int $id)
    {
        $this->id = $id;
    }
    public function setEntryDate($entryDate)
    {
        $this->entry_date = $entry_date;
    }
    public function setSerialNumber(string $serialNumber)
    {
        $this->serial_number = $serialNumber;
    }
    public function setPhysicalLocation(string $physicaLocation)
    {
        $this->physical_location = $physicaLocation;
    }
    public function setWarrantyDuration(int $warrantyDuration)
    {
        $this->warranty_duration = $warrantyDuration;
    }
    public function setStartWarrantyDate($startWarrantyDate)
    {
        $this->start_warranty_date = $startWarrantyDate;
    }
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
    public function setAcquisitionDate($acquisitionDate)
    {
        $this->acquisition_date = $acquisitionDate;
    }
    public function setPrice(int $price)
    {
        $this->price = $price;
    }
    public function setIdUnitType(int $idUnitType)
    {
        $this->id_unit_type = $idUnitType;
    }
    public function setIdVendor(int $idVendor)
    {
        $this->id_vendor = $idVendor;
    }
    public function setIdOS(int $idOS)
    {
        $this->id_OS = $idOS;
    }
    public function setIdStorageCapacity(int $idStorageCapacity)
    {
        $this->id_storage_capacity = $idStorageCapacity;
    }
    public function setIdScreenSize(int $idScreenSize)
    {
        $this->id_screen_size = $idScreenSize;
    }
    public function setIdRAMSize(int $idRAMSize)
    {
        $this->id_RAM_size = $idRAMSize;
    }
    public function setIdCPUType(int $idCPUType)
    {
        $this->id_CPU_type = $idCPUType;
    }
    public function setIdGivenTo(int $idGivenTo)
    {
        $this->id_given_to = $idGivenTo;
    }


    // Getters
    public function getId()
    {
        return $this->id = $id;
    }

    public function getEntryDate()
    {
        return $this->entry_date;
    }
    public function getSerialNumber()
    {
        return $this->serial_number;
    }
    public function getPhysicalLocation()
    {
        return $this->physical_location;
    }
    public function getWarrantyDuration()
    {
        return $this->warranty_duration;
    }
    public function getStartWarrantyDate()
    {
        return $this->start_warranty_date;
    }
    public function getDescription()
    {
        return $this->description;
    }
    public function getAcquisitionDate()
    {
        return $this->acquisition_date;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function getIdUnitType()
    {
        return $this->id_unit_type;
    }
    public function getIdVendor()
    {
        return $this->id_vendor;
    }
    public function getIdOS()
    {
        return $this->id_OS;
    }
    public function getIdStorageCapacity()
    {
        return $this->id_storage_capacity;
    }
    public function getIdScreenSize()
    {
        return $this->id_screen_size;
    }
    public function getIdRAMSize()
    {
        return $this->id_RAM_size;
    }
    public function getIdCPUType()
    {
        return $this->id_CPU_type;
    }
    public function getIdGivenTo()
    {
        return $this->id_given_to;
    }
    
}
